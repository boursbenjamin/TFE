class Inputs {

    constructor() {

        this.leftPressed = false;
        this.leftActive = false;
        this.rightPressed = false;
        this.rightActive = false;
        this.jumpPressed = false;
        this.jumpActive = false;

        window.addEventListener("keydown", function(e) {

            switch (e.key) {
                case 'ArrowLeft':

                    if (!this.leftActive) {

                        this.leftPressed = true;
                        this.leftActive = true;

                    } else {
                        this.leftPressed = false;
                    }

                    break;
                case 'ArrowRight':

                    if (!this.rightActive) {

                        this.rightPressed = true;
                        this.rightActive = true;

                    } else {
                        this.rightPressed = false;
                    }

                    break;

                case 'ArrowUp':

                    // if (!this.jumpActive) {
                    //
                    //     this.jumpPressed = true;
                    //     this.jumpActive = true;
                    //
                    // } else {
                    //     this.jumpPressed = false;
                    // }

                    this.jumpPressed = true;


                    break;

            }

        }.bind(this))

        window.addEventListener("keyup", function(e) {

            switch (e.key) {
                case 'ArrowLeft':

                    this.leftActive = false;

                    break;
                case 'ArrowRight':

                    this.rightActive = false;

                    break;

                case 'ArrowUp':

                    // this.jumpActive = false;
                    this.jumpPressed = false;

                    break;


            }

        }.bind(this))

    }

}
