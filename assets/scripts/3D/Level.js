class Level extends THREE.Group {
    constructor() {

        super();
        this.name = "test";

        // some decor element
        const geometry = new THREE.PlaneGeometry( 5, 50, 32 );
        const material = new THREE.MeshBasicMaterial({color: 0xffff00, side: THREE.DoubleSide});
        this.plane = new THREE.Mesh( geometry, material );
        this.add( this.plane );

        this.borders = [];

        // some border shape
        const geometryBorder = new THREE.BoxGeometry( 50, 100, 1 );
        const geometryFloor = new THREE.BoxGeometry(200, 1, 1);
        const materialBorder = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide});

        this.floor = new Border(geometryFloor, material);
        this.floor.setPosition(0, 0, 0);
        this.borderLeft = new Border(geometryBorder, materialBorder);
        this.borderLeft.setPosition(-100,0,0);
        this.borderRight = new Border(geometryBorder, materialBorder);
        this.borderRight.setPosition(100,0,0);
        this.add(this.borderLeft);
        this.add(this.borderRight);
        this.add(this.floor);
        this.borders.push(this.borderLeft);
        this.borders.push(this.borderRight);
        this.borders.push(this.floor);

    }

    update() {

        this.plane.position.x += 0.1;

    }
}
