class Camera extends THREE.PerspectiveCamera {

    constructor ({
        fov = 45,
        aspectRatio = window.innerWidth / window.innerHeight,
        closestVisible = 0.1,
        fartherVisible = 10000
    } = {}) {

        super(
            fov,
            aspectRatio,
            closestVisible,
            fartherVisible
        );

        this.position.set( 0, 0, 500 );
        this.lookAt(new THREE.Vector3(0,0,0));

    }

}
