class LevelController extends THREE.Group {
    constructor() {

        super();

        this.levels = [];

        // TODO: déclarer les levels ici, et les ajouter a levels
        const level = new Level();
        this.levels.push(level);

    }

    loadLevel(name) {

        for (let i in this.levels) {

            if (this.levels[i].name === name) {

                this.add(this.levels[i]);

            }

        }

    }

    unloadLevel(name) {

        for (let i in this.children) {

            if (this.children[i].name === name) {

                this.remove(this.children[i]);

            }

        }

    }

    update() {

        for (let i in this.children) {

            if (this.children[i].update) {

                this.children[i].update();

            }

        }

    }

    getBorderFromActualLevel() {
        return this.levels[0].borders;
    }

}
