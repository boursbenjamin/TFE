class SocketManager {

    constructor(menuController, io) {

        this.socket = io();

        this.menuController = menuController;

        this.menuController.getSocket(this);

        this.socket.on("connect", function() {
            console.log("connected");
        });

        this.socket.on("message", function(message) {
            console.log(message);
        });

        this.socket.on("searchingForGame", function() {

            this.menuController.searchingForGame();

        }.bind(this));

        this.socket.on("match", function() {

            this.menuController.matchingGame();

        }.bind(this));


        this.socket.on("positionShadow", function(position) {

            this.player.sphere.position.copy(position);


        }.bind(this));

        this.socket.on("positionLight", function(position) {

            this.player.sphere.position.copy(position);

        }.bind(this));

    }

    emitReadyToPlay(faction) {
        this.faction = faction;
        this.socket.emit("userReadyToPlay", faction);
    }

    emitPosition(position) {
        this.socket.emit("position", position);
    }

    getPlayer(player) {
        this.player = player;
    }

}
