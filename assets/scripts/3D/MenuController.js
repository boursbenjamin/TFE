class MenuController {

    constructor(domElement) {

        this.faction;

        this.menuDomElement = domElement;

        this.menuDomElement.buttonPlay.addEventListener("click", this.clickedOnPlay.bind(this));

        this.menuDomElement.buttonShadow.addEventListener("click", this.clickedOnShadow.bind(this));
        this.menuDomElement.buttonLight.addEventListener("click", this.clickedOnLight.bind(this));

    }

    searchingForGame () {
        this.menuDomElement.menuFaction.classList.add("close");
        this.menuDomElement.menuFaction.classList.remove("open");
        this.menuDomElement.menuWaiting.classList.add("open");
        this.menuDomElement.menuWaiting.classList.remove("close");
    }

    matchingGame() {

        this.menuDomElement.menuHome.classList.add("close");
        this.menuDomElement.menuHome.classList.remove("open");
        this.menuDomElement.menuFaction.classList.add("close");
        this.menuDomElement.menuFaction.classList.remove("open");
        this.menuDomElement.menuWaiting.classList.add("close");
        this.menuDomElement.menuWaiting.classList.remove("open");

        const app = new App(this.faction);

        app.start(this.socketManager);

    }

    clickedOnPlay() {
        this.menuDomElement.menuHome.classList.add("close");
        this.menuDomElement.menuHome.classList.remove("open");
        this.menuDomElement.menuFaction.classList.add("open");
        this.menuDomElement.menuFaction.classList.remove("close");
    }

    clickedOnShadow() {
        this.socketManager.emitReadyToPlay("shadow");
        this.faction = "shadow";
    }

    clickedOnLight() {
        this.socketManager.emitReadyToPlay("light");
        this.faction = "light";
    }

    getSocket(socket) {
        this.socketManager = socket;
    }

}
