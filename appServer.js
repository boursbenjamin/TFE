////////////////////////////////////////////////////
//// HTTP server with express
////////////////////////////////////////////////////

var express = require("express");
var app = express();
var http = require('http').Server(app);

app.use('/scripts' ,express.static('/Users/benjaminbours/Documents/perso/TFE/code/build/scripts/'));
app.use('/styles' ,express.static('/Users/benjaminbours/Documents/perso/TFE/code/build/styles/'));
app.use('/fonts' ,express.static('/Users/benjaminbours/Documents/perso/TFE/code/build/fonts/'));

app.get('/', function(req, res){
  res.sendFile('/Users/benjaminbours/Documents/perso/TFE/code/build/index.html');
});

////////////////////////////////////////////////////
//// SOCKET IO
////////////////////////////////////////////////////

var io = require('socket.io')(http);

var session = require("express-session")({
    secret : "my-secret",
    resave : true,
    saveUninitialized: true
});

////////////////////////////////////////////////////
////Express-SOCKET.IO-session
////////////////////////////////////////////////////

var sharedsession = require('express-socket.io-session');

app.use(session);

io.use(sharedsession(session, {
    autoSave:true
}));

// TODO: faire en sorte de pouvoir grouper les gens, faire un systeme de match making.

var gameCollection = {

    totalGameCount : 0,
    gameList : []

};

var gamePlaying = {
    totalGameCount : 0,
    gameList : []
};

var clients = {
    totalClientCount : 0,
    clientsList : []
};

// var playerData = {
//     id,
//
// }

var idConnection;
// console.log(gameCollection);

io.on('connection', function(socket){

    socket.emit("connect");

    console.log('user join the connected room');
    socket.join('connectedRoom');

    idConnection = socket.id;

    socket.handshake.session.userdata = {
        id : idConnection
    };
    //

    // TODO: chaque objet de game a un id unique qui correspond a un joueur. quand ça match pour un gars, selectionner l'id de la game ou ça marche,selectionner le player one déjà dans la game, et lui emettre l'évènement également.

    //     // TODO: faire passer un objet d'instance de game avec les datas néssécaires, le choix de fation, mais les positions des joueurs aussi dans l'idéal.

    var shadowPosition;


    socket.on("userReadyToPlay", function(faction) {

        // about the player
        var player;

        // var gameCreated = gameCollection.gameList;

        if (faction === "shadow") {

            socket.handshake.session.userdata.faction = faction;
            // socket.handshake.session.save();

            // console.log(socket.handshake.session);


            // si il existe déjà une instance de game avec le choix opposé, on le met dans la même instance que le mec, sinon on créer une nouvelle instance avec le choix.

        }

        else if ( faction === "light") {

            socket.handshake.session.userdata.faction = faction;
            // socket.handshake.session.save();

            // console.log(socket.handshake.session);

        }

        // about the game instance
        if (gameCollection.gameList.length === 0) {

            console.log('create a new game instance waiting for match');

            // TODO: mettre le player dans une room bien particulière qui correspondra a un ID unique pour cette game.

            gameCollection.gameList.push({
                id : socket.handshake.session.userdata.id,
                players : {
                    one : socket.handshake.session.userdata
                }
            });

            socket.join('waitingRoom');
            io.to('waitingRoom').emit('searchingForGame');
            console.log('WAITING FOR MATCH A GAME');
            console.log(gameCollection.gameList);

        }

        else {

            for (var i = 0; i < gameCollection.gameList.length; i++) {

                if (gameCollection.gameList[i].players.one.faction != socket.handshake.session.userdata.faction) {

                    console.log("match a game");

                    gameCollection.gameList[i].players.two = socket.handshake.session.userdata;

                    console.log(gameCollection.gameList[i].id);

                    // TODO: réussir a selectionner les sockets en attente pour les mettres dans la game room avec la personne qui match.

                    var one = gameCollection.gameList[i].players.one.id;
                    var two = gameCollection.gameList[i].players.two.id;


                    gamePlaying.gameList.push(gameCollection.gameList[i]);

                    socket.handshake.session.userdata.room = gameCollection.gameList[i].id;
                    socket.handshake.session.save();

                    io.to(one).emit('match');
                    io.to(two).emit('match');

                    console.log('a game started');

                    // TODO: quand ça a match, le détecter et le serveur envoie le signal au 2 clients de lancer l'app.

                    // console.log(gameCollection.gameList);
                    console.log(gamePlaying.gameList);

                    function ejectGame (element) {

                        return element.id != gameCollection.gameList[i].id;

                    }

                    var restGameWaiting = gameCollection.gameList.filter(ejectGame);

                    gameCollection.gameList = restGameWaiting;

                    return;

                }
            }

            gameCollection.gameList.push({
                id : socket.handshake.session.userdata.id,
                players : {
                    one : socket.handshake.session.userdata
                }
            });

            console.log(gameCollection.gameList);

            socket.join('waitingRoom');
            io.to('waitingRoom').emit('searchingForGame');
            console.log("WAITING FOR MATCH A GAME");

        }


    });

    socket.on("position", function(position) {

        // shadowPosition = position;
        // console.log(shadowPosition);

        // TODO: les joueuurs doivent avoir en commun une sorte d'id commun, afin de pouvoir s'envoyer des données entre eux, et entre eux uniquement.
        // console.log(socket.handshake.session.userdata.id);

        // var playerOne;
        // var playerTwo;
//
        for (var i = 0; i < gamePlaying.gameList.length; i++) {

            if (gamePlaying.gameList[i].players.one.id === socket.id) {

                console.log(gamePlaying.gameList[i].players.one.faction );
                if (gamePlaying.gameList[i].players.one.faction === "shadow") {

                    io.to(gamePlaying.gameList[i].players.two.id).emit('positionShadow', position);
                    // console.log("voici l'idea de l'autre joueur", gamePlaying.gameList[i].players.two.id);


                } else {
                    io.to(gamePlaying.gameList[i].players.two.id).emit('positionLight', position);
                }


            }

            if (gamePlaying.gameList[i].players.two.id === socket.id) {
                console.log(gamePlaying.gameList[i].players.two.faction );

                if (gamePlaying.gameList[i].players.two.faction === "shadow") {

                    io.to(gamePlaying.gameList[i].players.one.id).emit('positionShadow', position);
                    // console.log("voici l'idea de l'autre joueur", gamePlaying.gameList[i].players.two.id);


                } else {
                    io.to(gamePlaying.gameList[i].players.one.id).emit('positionLight', position);
                }
                // console.log("voici l'idea de l'autre joueur", gamePlaying.gameList[i].players.one.id);
                // io.to(gamePlaying.gameList[i].players.one.id).emit('positionShadow', shadowPosition);

            }

        }

        // socket

    });

    socket.on('disconnect', function(){
        socket.leave('connectedRoom');
        console.log('user leave the connected room');

        function testFilter (element) {

            return element.id != socket.handshake.session.userdata.id;

        }

        var restGame = gameCollection.gameList.filter(testFilter);
        gameCollection.gameList = restGame;

    });

    // socket.broadcast.emit('message', "Un autre client vient de se connecter !");

});

http.listen(8080, function(){
  console.log('listening on *:8080');
});
