var gulp = require('gulp'),
    spawn = require('child_process').spawn,
    node,

    sass = require('gulp-sass'),
    babel = require('gulp-babel'),
    watch = require('gulp-watch'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    sourcemaps = require('gulp-sourcemaps'),
    manifest = require('asset-builder')('./assets/manifest.json'),
    browserSync = require('browser-sync').create(),

    indexHTMLpath = manifest.getDependencyByName('html.js'),
    mainJSpath = manifest.getDependencyByName('main.js'),
    mainCSSpath = manifest.getDependencyByName('main.css'),
    imagespath = manifest.getDependencyByName('images.js'),
    fontspath = manifest.getDependencyByName('fonts.js'),
    objspath = manifest.getDependencyByName('objs.js'),
    texturespath = manifest.getDependencyByName('textures.js'),

    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    del = require("del"),
    runSequence = require("run-sequence"),

    uglify = require('gulp-uglify'),
    gulpIf = require('gulp-if'),
    cssnano = require('gulp-cssnano');

// Development task //////////////////////

gulp.task('test',function() {
    console.log(imagespath);
})

gulp.task("scripts", function() {

    return gulp.src(mainJSpath.globs)
      .pipe(sourcemaps.init())
      .pipe(concat(mainJSpath.name))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(manifest.paths.buildJS));

});

gulp.task("obj", function() {

    return gulp.src(objspath.globs)
      .pipe(gulp.dest(manifest.paths.buildObj));

});

gulp.task('sass', function(){
  return gulp.src(mainCSSpath.globs)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError)) // Using gulp-sass
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(manifest.paths.buildCSS))
    .pipe(browserSync.reload({
        stream: true
    }))
});

gulp.task("html", function(){

    return gulp.src(indexHTMLpath.globs)
    .pipe(gulp.dest(manifest.paths.build));
});

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'build'
    },
  })
});

// Gulp watch syntax
// gulp.watch('files-to-watch', ['tasks', 'to', 'run']);

gulp.task("watch",["browserSync", "sass"], function() {

    gulp.watch('assets/styles/*.sass', ['sass']);
    gulp.watch('assets/*.html',['html', browserSync.reload]);
    gulp.watch('assets/scripts/**/*.js', ['scripts', browserSync.reload]);

});

// gulp.task("develop", function(callback){
//     runSequence(
//         ["sass","html", "scripts", "browserSync", "watch"],
//         callback
//     )
// });

/////////////////////////////////////////

// Server task /////////////////////

gulp.task("server", function() {

    if (node) node.kill()
  node = spawn('node', ['appServer.js'], {stdio: 'inherit'})
  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });

});

/////////////////////////////////////////


// Compilation task /////////////////////

gulp.task('images', function(){
  return gulp.src(imagespath.globs)
  .pipe(cache(imagemin({
      // some options
      interlaced: true
  })))
  .pipe(gulp.dest(manifest.paths.buildImages))
});

gulp.task('textures', function(){
  return gulp.src(texturespath.globs)
  // .pipe(cache(imagemin({
  //     // some options
  //     interlaced: true
  // })))
  .pipe(gulp.dest(manifest.paths.buildTextures))
});

gulp.task("fonts", function() {
    return gulp.src(fontspath.globs)
    .pipe(gulp.dest(manifest.paths.buildFonts));
});

gulp.task("compress-code", function() {
    return gulp.src(manifest.paths.buildCompress)
        .pipe(gulpIf("*.js", minify()))
        .pipe(gulpIf("*.css", cssnano()))
        .pipe(gulp.dest("build/compressed"))
})

gulp.task("build", function(callback){
    runSequence("clean-build",
        ["sass","html","scripts", "images", "fonts", "obj", "textures"],
        // "compress-code",
        callback
    )
});

/////////////////////////////////////////

// Usefull task /////////////////////////

gulp.task('clean-build', function() {
  return del.sync('build');
});

gulp.task('cache-clear', function (callback) {

    return cache.clearAll(callback)

});

/////////////////////////////////////////
